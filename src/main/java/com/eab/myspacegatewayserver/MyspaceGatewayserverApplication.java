package com.eab.myspacegatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;

@SpringBootApplication
public class MyspaceGatewayserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyspaceGatewayserverApplication.class, args);
    }

    @Bean
    public RouteLocator myspaceRouteConfig(RouteLocatorBuilder routeLocatorBuilder) {
        return routeLocatorBuilder.routes()
                .route(p -> p
                        .path("/myspace/products/**")
                        .filters(f -> f.rewritePath("/myspace/products/(?<segment>.*)", "/${segment}")
                                .addResponseHeader("X-Response-Time", LocalDateTime.now().toString())
                                .circuitBreaker(config -> config.setName("productsCircuitBreaker")
                                        .setFallbackUri("forward:/api/contactSupport")))
                        .uri("lb://PRODUCTS")).build();
    }

}
